/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

#define SWAP(X, Y, TEMP) ((TEMP) = (X), (X) = (Y), (Y) = (TEMP))

typedef struct {
	int value;
	} Element;

void heapSort(Element a[], int n);
void adjust(Element a[], int root, int n);
void printArray(Element *a, int size);

int step = 1;

int main()
{
	FILE*		pInputFile = fopen("input.txt", "r");
	FILE*		pOutputFile;
	Element*	list = NULL;
	int			size;
	int			i;

	/* input data */
	printf("<<<<<<<<<< Input List >>>>>>>>> \n");
	fscanf(pInputFile, "%d", &size);
	list = (Element*)malloc(sizeof(Element) * (size + 1));

	for (i = 1; i <= size; i++) {
		fscanf(pInputFile, "%d ", &list[i]);
		printf("%d ", list[i]);
	}

	fclose(pInputFile);

	/* heap sort */
	printf("\n\n<<<<< executing heap sort >>>>>\n");
	heapSort(list, size);

	/* print result */
	printf("\n<<<<< Sorted List >>>>> \n");
	printArray(list, size);

	/* save data */
	pOutputFile = fopen("output.txt", "w");
	for (i = 1; i <= size; i++) {
		fprintf(pOutputFile, "%d ", list[i]);
	}

	/* memory free */
	free(list);

	return 0;
}

/**
 * perform a heap sort on a[1:n]
 */
void heapSort(Element a[], int n)
{
	int		i, j;
	Element	temp;

	for (i = n / 2; i > 0; i--) {
		adjust(a, i, n);
	}
	printf("after initialization of max heap... \n");
	printArray(a, n);
	for (i = n - 1; i > 0; i--) {
		SWAP(a[1], a[i + 1], temp);
		adjust(a, 1, i);
		printf("step %2d : ", step);
		step++;
		printArray(a, n);
	}
}

/**
 * adjust the binary tree to establish the heap
 */
void adjust(Element a[], int root, int n)
{
	int		child, rootkey;
	Element temp = a[root];
	rootkey = a[root].value;
	child = 2 * root;

	while (child <= n) {
		if ((child < n) && (a[child].value < a[child + 1].value)) {
			child++;
		}
		/* compare root and max. child*/
		if (rootkey > a[child].value) {
			break;
		}
		else {
			/* move to parent */
			a[child / 2] = a[child];
			child *= 2;
		}
	}

	a[child / 2] = temp;
}

void printArray(Element *a, int size)
{
	int i;

	for (i = 1; i <= size; i++) {
		printf("%2d ", a[i].value);
	}
	printf("\n");
}