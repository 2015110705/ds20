/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int value;
	} Element;

void merge(Element initList[], Element mergedList[], int i, int m, int n);
void mergePass(Element initList[], Element mergedList[], int n, int s);
void mergeSort(Element **a, int n);
void printResult(Element** a, int arrSize, int segSize);

int main()
{
	FILE*		pInputFile = fopen("input.txt", "r");
	Element**	list = NULL;
	int			size;
	int			i, j;

	/* input data */
	printf("<<<<<<<<<< Input List >>>>>>>>>> \n");
	fscanf(pInputFile, "%d ", &size);

	list = (Element**)malloc(sizeof(Element) * 2);
	
	for (i = 0; i < 2; i++) {
		list[i] = (Element*)malloc(sizeof(Element) * (size + 1));	
	}
	for (i = 1; i <= size; i++) {
		fscanf(pInputFile, "%d ", &list[0][i]);
		printf("%d ", list[0][i]);
	}

	fclose(pInputFile);

	/* merge sorting */
	printf("\n\n<<<<<<<<<< executing iterative merge sort >>>>>>>>>> \n");
	mergeSort(list, size);

	/* print result */
	printf("<<<<<<<<<< Sorted List >>>>>>>>>> \n");
	for (i = 1; i <= size; i++) {
		printf("%2d ", list[0][i]);
	}

	/* memory free */
	for (i = 0; i < 2; i++) {
		free(list[i]);
	}
	free(list);

	return 0;
}

/**
 * the sorted lists initList[i:m] and initList[m+1:n] are
 * merged to obtain the sorted list mergedList[i:n]
 */
void merge(Element initList[], Element mergedList[], int i, int m, int n)
{
	int j = m + 1;
	int k = i;
	int t;

	while (i <= m && j <= n) {
		if (initList[i].value <= initList[j].value) {
			mergedList[k++] = initList[i++];
		}
		else {
			mergedList[k++] = initList[j++];
		}
	}

	if (i > m) {
		/* mergedList[k:n] = initList[j:n] */
		for (t = j; t <= n; t++) {
			mergedList[t] = initList[t];
		}
	}
	else {
		/* mergedList[k:n] = initList[i:m] */
		for (t = i; t <= m; t++) {
			mergedList[k + t - i] = initList[t];
		}
	}
}

/**
 * perform one pass of the merge sort, merge adjacent
 * pairs of sorted segments from initList[] into mergedList[],
 * @param	: n is the number of elements in the list
 * @param	: s is the size of each sorted segment
 */
void mergePass(Element initList[], Element mergedList[], int n, int s)
{
	int i, j;
	
	for (i = 1; i <= n - 2 * s + 1; i += 2 * s) {
		merge(initList, mergedList, i, i + s - 1, i + 2 * s - 1);
	}
	if (i + s - 1 < n) {
		merge(initList, mergedList, i, i + s - 1, n);
	}
	else {
		for (j = i; j <= n; j++) {
			mergedList[j] = initList[j];
		}
	}
}

/**
 * sort a[1:n] using the merge sort method
 * @param	: array a
 * @param	: array size n
 */
void mergeSort(Element **a, int n)
{
	int s = 1;	/* current segment size */
	int k = 0;

	while (s < n) {
		mergePass(a[k], a[1 - k], n, s);
		printResult(a, n, s);
		s *= 2;
		k = 1 - k;
	}
}

/**
 * print array's elements
 * @param	: array a
 * @param	: array size
 * @param	: segment size
 */
void printResult(Element** a, int arrSize, int segSize)
{
	int i;

	printf("%13s : %d\n", "segment size", segSize);
	printf("%13s : ", "a");
	for (i = 1; i <= arrSize; i++) {
		printf("%2d ", a[0][i]);
	}
	printf("\n%13s : ", "extra");
	for (i = 1; i <= arrSize; i++) {
		printf("%2d ", a[1][i]);
	}
	printf("\n\n");
}