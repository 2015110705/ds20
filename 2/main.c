/**
 * 2015110705 김민규
 * 본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.
 */
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int value;
	} Element;

int rmergeSort(Element a[], int link[], int left, int right);
int listMerge(Element a[], int link[], int start1, int start2);
void printList(Element a[], int link[], int size);

int main()
{
	FILE*		pInputFile = fopen("input.txt", "r");
	Element*	list = NULL;
	int*		link = NULL;
	int			size;
	int			iter;
	int			i;

	/* input data */
	printf("<<<<<<<<<< Input List >>>>>>>>>> \n");
	fscanf(pInputFile, "%d ", &size);

	list = (Element*)malloc(sizeof(Element) * (size + 1));
	link = (int*)malloc(sizeof(int) * (size + 1));

	for (i = 1; i <= size; i++) {
		fscanf(pInputFile, "%d ", &list[i]);
		printf("%d ", list[i]);
	}

	for (i = 0; i <= size; i++) {
		link[i] = 0;
	}

	fclose(pInputFile);

	/* recursive merge sort */
	printf("\n\n<<<<< executing recursive merge sort >>>>> \n");
	rmergeSort(list, link, 1, size);
	
	/* print result */
	printList(list, link, size);

	/* memory free */
	free(link);
	free(list);

	return 0;
}

/**
 * link[i] is initially 0 for all i
 * @param	: a[left:right] is to be sorted
 * @param	: link has index
 * @return	: the index of the first element in the sorted chain
 */
int rmergeSort(Element a[], int link[], int left, int right)
{
	int mid;
	
	if (left >= right) {
		return left;
	}
	printf("left : %d, right : %d\n", left, right);
	mid = (left + right) / 2;

	return listMerge(a, link,
		/* sort left half */
		rmergeSort(a, link, left, mid),
		/* sort right half */
		rmergeSort(a, link, mid + 1, right));
}

/** 
 * sorted chains begining at start1 and start2
 * respectively, are merged; link[0] is used as a temporary header 
 * @return	: returns start of merged chain
 */
int listMerge(Element a[], int link[], int start1, int start2)
{
	int last1, last2, lastResult = 0;
	
	for (last1 = start1, last2 = start2; last1 && last2;) {
		if( a[last1].value <= a[last2].value ) {
			link[lastResult] = last1;
			lastResult = last1;
			last1 = link[last1];
		}
		else {
			link[lastResult] = last2;
			lastResult = last2;
			last2 = link[last2];
		}
	}

	/* attach remaining records to result chain */
	if (last1 == 0) {
		link[lastResult] = last2;
	}
	else {
		link[lastResult] = last1;
	}

	return link[0];
}

/**
 * print array's elements
 * @param	: array a
 * @param	: link
 * @param	: array size
 */
void printList(Element a[], int link[], int size)
{
	int iter = link[0];
	int i;

	printf("\n<<<<< Sorted List >>>>> \n");
	for (i = 1; i <= size; i++) {
		printf("%d ", a[iter]);
		iter = link[iter];
	}
	printf("\n");
}